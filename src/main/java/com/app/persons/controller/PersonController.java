/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.controller;

import com.app.persons.constants.URL_API;
import com.app.persons.entities.PersonEntity;
import com.app.persons.model.PersonModel;
import com.app.persons.model.ResponseModel;
import com.app.persons.service.PersonService;
import com.app.persons.util.BuilderEntity;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Permafrost
 */
@RestController
@RequestMapping(URL_API.PERSON_BASE)
public class PersonController {

    private static final Logger LOG = Logger.getLogger(PersonController.class.getName());
    @Autowired
    PersonService personService;

    @PostMapping("/addNewPerson")
    public ResponseModel addNewPerson(@Valid @RequestBody PersonModel personModel) {
        ResponseModel responseModel = null;
        try {
            PersonEntity personEntity = BuilderEntity.constructPersonEntity(personModel);
            personEntity = personService.create(personEntity);
            responseModel = new ResponseModel("Nueva persona insertada", 200, personEntity);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se Creaba la nueva persona", 500);
            LOG.log(Level.WARNING, "Error in PersonController.addNewPerson() {0}", ex);
        }
        return responseModel;

    }

    @DeleteMapping("/deletePerson/{id}")
    public ResponseModel deletePerson(@PathVariable int id) {
        ResponseModel responseModel = null;
        try {
            PersonEntity personEntity = new PersonEntity(id);
            personService.delete(personEntity);
            responseModel = new ResponseModel("Persona Borrada", 200, personEntity);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se Borraba la persona", 500);
            LOG.log(Level.WARNING, "Error in PersonController.deletePerson() {0}", ex);
        }
        return responseModel;
    }

    @PutMapping("/updatePerson/{id}")
    public ResponseModel updatePerson(@PathVariable int id, @RequestBody PersonModel personModel) {
        ResponseModel responseModel = null;
        try {
            PersonEntity personEntity = BuilderEntity.constructPersonEntity(personModel);
            personEntity.setIdPerson(id);
            personService.update(personEntity);
            responseModel = new ResponseModel("Persona Actualizada", 200, personEntity);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se Borraba la persona", 500);
            LOG.log(Level.WARNING, "Error in PersonController.updatePerson() {0}", ex);
        }
        return responseModel;
    }

    @GetMapping("/getPerson/{id}")
    public ResponseModel getPerson(@PathVariable int id) {
        ResponseModel responseModel = null;
        try {
            Optional<PersonEntity> person = personService.read(id);
            responseModel = new ResponseModel("Persona Buscada", 200, person);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se buscaba la persona", 500);
            LOG.log(Level.WARNING, "Error in PersonController.getPerson() {0}", ex);
        }
        return responseModel;
    }

    @GetMapping("/getAllPersons")
    public ResponseModel getAllPersons() {
        ResponseModel responseModel = null;
        try {
            List<PersonEntity> persons = personService.readAllPersons();
            responseModel = new ResponseModel("Lista de Personas", 200, null, persons);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se buscaban las personas", 500);
            LOG.log(Level.WARNING, "Error in PersonController.getAllPersons() {0}", ex);
        }
        return responseModel;
    }

}
