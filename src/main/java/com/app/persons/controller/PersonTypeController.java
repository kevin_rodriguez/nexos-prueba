/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.controller;

import com.app.persons.constants.URL_API;
import com.app.persons.entities.PersonEntity;
import com.app.persons.entities.PersonsTypeEntity;
import com.app.persons.model.ResponseModel;
import com.app.persons.service.PersonTypeService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Permafrost
 */
@RestController
@RequestMapping(URL_API.PERSON_TYPE_BASE)
public class PersonTypeController {

    private static final Logger LOG = Logger.getLogger(PersonTypeController.class.getName());

    @Autowired
    PersonTypeService personTypeService;

    @GetMapping("/getAll")
    public ResponseModel getAllTypesPersons() {
        ResponseModel responseModel = null;
        try {
            List<PersonsTypeEntity> personsTypes = personTypeService.getAllPersonsTypes();
            responseModel = new ResponseModel("Lista de Tipos Familiares", 200, null, personsTypes);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se buscaban los tipos familiares", 500);
            LOG.log(Level.WARNING, "Error in PersonController.getAllTypesPersons() {0}", ex);
        }
        return responseModel;
    }

}
