/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.controller;

import com.app.persons.constants.URL_API;
import com.app.persons.entities.RelationshipEntity;
import com.app.persons.model.LinkRelationRowMapper;
import com.app.persons.model.RelationshipModel;
import com.app.persons.model.ResponseModel;
import com.app.persons.service.RelationshipService;
import com.app.persons.util.BuilderEntity;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Permafrost
 */
@RestController
@RequestMapping(URL_API.RELATIONSHIP_BASE)
public class RelationshipController {

    private static final Logger LOG = Logger.getLogger(RelationshipController.class.getName());

    @Autowired
    RelationshipService relationshipService;

    @PostMapping("/addNewRelation")
    public ResponseModel addNewRelation(@RequestBody RelationshipModel relationshipModel) {
        ResponseModel responseModel = null;
        try {
            RelationshipEntity relationshipEntity = BuilderEntity.constructRelationshipEntity(relationshipModel);
            relationshipEntity = relationshipService.create(relationshipEntity);
            responseModel = new ResponseModel("Nueva Relacion insertada", 200, relationshipEntity);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se Creaba la nueva relacion", 500);
            LOG.log(Level.WARNING, "Error in PersonController.addNewRelation() {0}", ex);
        }
        return responseModel;
    }

    @DeleteMapping("/deleteRelation/{id}")
    public ResponseModel deleteRelation(@PathVariable int id) {
        ResponseModel responseModel = null;
        try {
            RelationshipEntity relationshipEntity = new RelationshipEntity(id);
            relationshipService.delete(relationshipEntity);
            responseModel = new ResponseModel("Relacion  Borrada", 200, relationshipEntity);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se Borraba la relacion", 500);
            LOG.log(Level.WARNING, "Error in PersonController.deleteRelation() {0}", ex);
        }
        return responseModel;
    }

    @PutMapping("/updateRelation/{id}")
    public ResponseModel updateRelation(@PathVariable int id, @RequestBody RelationshipModel relationshipModel) {
        ResponseModel responseModel = null;
        try {
            RelationshipEntity relationshipEntity = BuilderEntity.constructRelationshipEntity(relationshipModel);
            relationshipEntity.setIdRelation(id);
            relationshipService.update(relationshipEntity);
            responseModel = new ResponseModel("Relacion Actualizada", 200, relationshipEntity);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se Borraba la relacion", 500);
            LOG.log(Level.WARNING, "Error in PersonController.updateRelation() {0}", ex);
        }
        return responseModel;
    }

    @GetMapping("/getRelation/{id}")
    public ResponseModel getRelation(@PathVariable int id) {
        ResponseModel responseModel = null;
        try {
            Optional<RelationshipEntity> relation = relationshipService.read(id);
            responseModel = new ResponseModel("Relacion Buscada", 200, relation);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se buscaba la relacion", 500);
            LOG.log(Level.WARNING, "Error in PersonController.getRelation() {0}", ex);
        }
        return responseModel;
    }

    @GetMapping("/getAllRelations/{id}")
    public ResponseModel getAllPersons(@PathVariable int id) {
        ResponseModel responseModel = null;
        try {
            List<LinkRelationRowMapper> relationLs = relationshipService.findAllRelationToIdPerson(id);
            responseModel = new ResponseModel("Lista de Personas", 200, null, relationLs);
        } catch (Exception ex) {
            responseModel = new ResponseModel("Error mientras Se buscaban las personas", 500);
            LOG.log(Level.WARNING, "Error in PersonController.getAllPersons() {0}", ex);
        }
        return responseModel;
    }

}
