/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.util;

import com.app.persons.entities.PersonEntity;
import com.app.persons.entities.RelationshipEntity;
import com.app.persons.model.PersonModel;
import com.app.persons.model.RelationshipModel;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author Permafrost
 */
public class BuilderEntity {

    public static PersonEntity constructPersonEntity(PersonModel personModel) {
        if (personModel != null) {
            Timestamp localDate = getLocalDate();
            PersonEntity personEntity = new PersonEntity();
            personEntity.setName(personModel.getName());
            personEntity.setLastname(personModel.getLastname());
            personEntity.setDateCreated(localDate);
            personEntity.setDateUpdate(localDate);
            personEntity.setUuidPerson(uuidToEntity());
            return personEntity;
        } else {
            return null;
        }

    }

    public static RelationshipEntity constructRelationshipEntity(RelationshipModel relationshipModel) {
        if (relationshipModel != null) {
            RelationshipEntity relationshipEntity = new RelationshipEntity();
            relationshipEntity.setIdPersonPrincipal(relationshipModel.getIdPersonPrincipal());
            relationshipEntity.setIdPersonSecondary(relationshipModel.getIdPersonSecondary());
            relationshipEntity.setIdPersonType(relationshipModel.getIdPersonType());
            relationshipEntity.setDateCreated(getLocalDate());
            relationshipEntity.setUuidRelationship(uuidToEntity());
            return relationshipEntity;
        } else {
            return null;
        }

    }

    private static Timestamp getLocalDate() {
        return new Timestamp(System.currentTimeMillis());
    }

    private static String uuidToEntity() {
        return UUID.randomUUID().toString();
    }

}
