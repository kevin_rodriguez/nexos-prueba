/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.security;

import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 *
 * @author Permafrost
 */
@Component
public class LoggerInterceptor implements HandlerInterceptor {

    private static final Logger LOG = Logger.getLogger(LoggerInterceptor.class.getName());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        String uriRequest = request.getRequestURI();
        String requestMethod = request.getMethod();        
        LOG.log(Level.INFO, "Trace Request uriRequest: {0}  requestMethod: {1} ", new Object[]{uriRequest, requestMethod});

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
        String uriRequest = request.getRequestURI();
        String requestMethod = request.getMethod();
        LOG.log(Level.INFO, "Trace Finish Success: uriRequest: {0}  requestMethod: {1} ", new Object[]{uriRequest, requestMethod});
    }

}
