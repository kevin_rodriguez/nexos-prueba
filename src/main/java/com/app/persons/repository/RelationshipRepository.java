/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.repository;

import com.app.persons.entities.RelationshipEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Permafrost
 */
@Repository
public interface RelationshipRepository extends JpaRepository<RelationshipEntity, Object> {

}
