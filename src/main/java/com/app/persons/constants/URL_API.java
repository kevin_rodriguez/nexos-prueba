/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.constants;

/**
 *
 * @author Permafrost
 */
public class URL_API {
    
    public static final String PERSON_BASE = "/api/v1/person";
    public static final String RELATIONSHIP_BASE = "/api/v1/relationship";
    public static final String PERSON_TYPE_BASE = "/api/v1/person-type";
     
    
}
