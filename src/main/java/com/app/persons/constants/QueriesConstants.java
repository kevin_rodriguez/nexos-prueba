/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.constants;

/**
 *
 * @author Permafrost
 */
public class QueriesConstants {

    public static final String ALL_RELATIONS_OF_USER_ID = "SELECT \n "
            + "p2.name name, \n "
            + "p2.lastname lastname, \n "
            + "pt.description relationDescription \n "
            + "FROM \n "
            + "  person p \n "
            + " inner join \n "
            + "   relationship r \n "
            + "  on p.id_person = r.id_person_principal \n "
            + "inner join \n "
            + "    person p2 \n "
            + "   on p2.id_person = r.id_person_secondary \n "
            + "inner join \n "
            + "   persons_type pt \n "
            + "   on r.id_person_type = pt.id_person_type \n "
            + "where\n "
            + "  p.id_person = :id ";
}
