/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Permafrost
 */
@Entity
@Table(name = "person")
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private int idPerson;

    @NotNull
    private String name;

    @NotNull
    private String lastname;

    @NotNull
    private Timestamp dateCreated;

    @NotNull
    private Timestamp dateUpdate;

    @Column(unique = true)
    @NotNull
    private String uuidPerson;

    public PersonEntity() {
    }

    public PersonEntity(int idPerson, String name, String lastname, Timestamp dateCreated, Timestamp dateUpdate, String uuidPerson) {
        this.idPerson = idPerson;
        this.name = name;
        this.lastname = lastname;
        this.dateCreated = dateCreated;
        this.dateUpdate = dateUpdate;
        this.uuidPerson = uuidPerson;
    }

    public PersonEntity(int idPerson) {
        this.idPerson = idPerson;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUuidPerson() {
        return uuidPerson;
    }

    public void setUuidPerson(String uuidPerson) {
        this.uuidPerson = uuidPerson;
    }

    public Timestamp getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Timestamp dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

}
