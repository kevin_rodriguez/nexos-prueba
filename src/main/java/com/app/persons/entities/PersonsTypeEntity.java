/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Permafrost
 */
@Entity
@Table(name = "persons_type")
public class PersonsTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private int idPersonType;

    @NotNull
    private String type;

    @NotNull
    private String description;

    @Column(unique = true)
    @NotNull
    private String uuidPersonType;

    public PersonsTypeEntity() {
    }

    public PersonsTypeEntity(int idPersonType, String type, String description, String uuidPersonType) {
        this.idPersonType = idPersonType;
        this.type = type;
        this.description = description;
        this.uuidPersonType = uuidPersonType;
    }

    public int getIdPersonType() {
        return idPersonType;
    }

    public void setIdPersonType(int idPersonType) {
        this.idPersonType = idPersonType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuidPersonType() {
        return uuidPersonType;
    }

    public void setUuidPersonType(String uuidPersonType) {
        this.uuidPersonType = uuidPersonType;
    }

}
