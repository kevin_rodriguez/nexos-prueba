/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Permafrost
 */
@Entity
@Table(name = "relationship")
public class RelationshipEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private int idRelation;

    @NotNull
    private int idPersonPrincipal;

    @NotNull
    private int idPersonType;
    
    @NotNull
    private int idPersonSecondary; 

    @NotNull
    private Timestamp dateCreated;

    @Column(unique = true)
    @NotNull
    private String uuidRelationship;

    public RelationshipEntity() {
    }

    public RelationshipEntity(int idRelation) {
        this.idRelation = idRelation;
    }
    
    

    public RelationshipEntity(int idRelation, int idPersonPrincipal, int idPersonType, int idPersonSecondary, Timestamp dateCreated, String uuidRelationship) {
        this.idRelation = idRelation;
        this.idPersonPrincipal = idPersonPrincipal;
        this.idPersonType = idPersonType;
        this.idPersonSecondary = idPersonSecondary;
        this.dateCreated = dateCreated;
        this.uuidRelationship = uuidRelationship;
    }

    public int getIdPersonPrincipal() {
        return idPersonPrincipal;
    }

    public void setIdPersonPrincipal(int idPersonPrincipal) {
        this.idPersonPrincipal = idPersonPrincipal;
    }

    public int getIdPersonSecondary() {
        return idPersonSecondary;
    }

    public void setIdPersonSecondary(int idPersonSecondary) {
        this.idPersonSecondary = idPersonSecondary;
    }

    public int getIdPersonType() {
        return idPersonType;
    }

    public void setIdPersonType(int idPersonType) {
        this.idPersonType = idPersonType;
    }

    public int getIdRelation() {
        return idRelation;
    }

    public void setIdRelation(int idRelation) {
        this.idRelation = idRelation;
    }

    public String getUuidRelationship() {
        return uuidRelationship;
    }

    public void setUuidRelationship(String uuidRelationship) {
        this.uuidRelationship = uuidRelationship;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

}
