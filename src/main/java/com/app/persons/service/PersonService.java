/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.service;

import com.app.persons.dao.GenericDao;
import com.app.persons.entities.PersonEntity;
import com.app.persons.repository.PersonRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Permafrost
 */
@Service
public class PersonService implements GenericDao<PersonEntity> {

    @Autowired
    PersonRepository personRepository;

    @Override
    public PersonEntity create(PersonEntity t) {
        return personRepository.save(t);

    }

    @Override
    public Optional<PersonEntity> read(int id) {
        return personRepository.findById(id);
    }

    public PersonEntity update(PersonEntity personEntity) {
        return personRepository.save(personEntity);
    }

    @Override
    public void delete(PersonEntity personEntity) {
        personRepository.delete(personEntity);
    }

    public List<PersonEntity> readAllPersons() {
        return personRepository.findAll();
    }

}
