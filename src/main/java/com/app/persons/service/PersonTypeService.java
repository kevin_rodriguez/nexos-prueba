/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.service;

import com.app.persons.entities.PersonsTypeEntity;
import com.app.persons.repository.PersonTypeRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Permafrost
 */
@Service
public class PersonTypeService {

    @Autowired
    PersonTypeRepository personTypeRepository;

    public List<PersonsTypeEntity> getAllPersonsTypes() {
        List<PersonsTypeEntity> personsTypels = new ArrayList<>();
        try {
            personsTypels = personTypeRepository.findAll();
        } catch (Exception ex) {

        }

        return personsTypels;
    }

}
