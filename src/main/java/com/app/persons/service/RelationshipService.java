/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.service;

import com.app.persons.constants.QueriesConstants;
import com.app.persons.controller.PersonController;
import com.app.persons.dao.GenericDao;
import com.app.persons.entities.RelationshipEntity;
import com.app.persons.model.LinkRelationRowMapper;
import com.app.persons.repository.RelationshipRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author Permafrost
 */
@Service
public class RelationshipService extends QueriesConstants implements GenericDao<RelationshipEntity> {

    private static final Logger LOG = Logger.getLogger(PersonController.class.getName());

    @Autowired
    RelationshipRepository relationshipRepository;

    @Autowired
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Override
    public RelationshipEntity create(RelationshipEntity t) {
        return relationshipRepository.save(t);
    }

    @Override
    public Optional<RelationshipEntity> read(int id) {
        return relationshipRepository.findById(id);
    }

    @Override
    public RelationshipEntity update(RelationshipEntity t) {
        return relationshipRepository.save(t);
    }

    @Override
    public void delete(RelationshipEntity t) {
        relationshipRepository.delete(t);
    }

    public List<LinkRelationRowMapper> findAllRelationToIdPerson(int id) {
        List<LinkRelationRowMapper> linkRelationRowMapper = new ArrayList<>();
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("id", id);
            linkRelationRowMapper = parameterJdbcTemplate.query(ALL_RELATIONS_OF_USER_ID, parameters, new BeanPropertyRowMapper(LinkRelationRowMapper.class));
        } catch (Exception ex) {
            LOG.log(Level.WARNING, "Error in RelationshipService.findAllRelationToIdPerson() {0}", ex);
        }

        return linkRelationRowMapper;
    }

}
