/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.dao;

import java.util.Optional;

/**
 *
 * @author Permafrost
 */
public interface GenericDao<T> {

    T create(T t);

    Optional<T> read(int id);

    T update(T t);

    void delete(T t);

}
