/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.model;

import java.util.List;

/**
 *
 * @author Permafrost
 */
public class ResponseModel<T> {

    private String resultMessage;
    private int resultCode;
    private T object;
    private List<T> lsObjects;

    public ResponseModel() {
    }

    public ResponseModel(String resultMessage, int resultCode) {
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
    }

    public ResponseModel(String resultMessage, int resultCode, T object) {
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
        this.object = object;        
    }

    public ResponseModel(String resultMessage, int resultCode, T object, List<T> lsObjects) {
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
        this.object = object;
        this.lsObjects = lsObjects;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public T getT() {
        return object;
    }

    public void setT(T object) {
        this.object = object;
    }

    public List<T> getLsT() {
        return lsObjects;
    }

    public void setLsT(List<T> lsObjects) {
        this.lsObjects = lsObjects;
    }

}
