/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.model;

/**
 *
 * @author Permafrost
 */
public class RelationshipModel {

    private int idPersonPrincipal;
    private int idPersonType;
    private int idPersonSecondary;

    public RelationshipModel() {
    }

    public RelationshipModel(int idPersonPrincipal, int idPersonType, int idPersonSecondary) {
        this.idPersonPrincipal = idPersonPrincipal;
        this.idPersonType = idPersonType;
        this.idPersonSecondary = idPersonSecondary;
    }

    public int getIdPersonPrincipal() {
        return idPersonPrincipal;
    }

    public void setIdPersonPrincipal(int idPersonPrincipal) {
        this.idPersonPrincipal = idPersonPrincipal;
    }

    public int getIdPersonType() {
        return idPersonType;
    }

    public void setIdPersonType(int idPersonType) {
        this.idPersonType = idPersonType;
    }

    public int getIdPersonSecondary() {
        return idPersonSecondary;
    }

    public void setIdPersonSecondary(int idPersonSecondary) {
        this.idPersonSecondary = idPersonSecondary;
    }
    
    

}
