/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.model;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author Permafrost
 */
public class PersonModel {

    @NotBlank
    private String name;

    @NotBlank
    private String lastname;


    public PersonModel() {
    }

    public PersonModel(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotBlank String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
