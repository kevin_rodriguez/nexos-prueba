/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.persons.model;

/**
 *
 * @author Permafrost
 */
public class LinkRelationRowMapper {

    private String name;
    private String lastname;
    private String relationDescription;

    public LinkRelationRowMapper() {
    }

    public LinkRelationRowMapper(String name, String lastname, String relationDescription) {
        this.name = name;
        this.lastname = lastname;
        this.relationDescription = relationDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRelationDescription() {
        return relationDescription;
    }

    public void setRelationDescription(String relationDescription) {
        this.relationDescription = relationDescription;
    }

}
